
def scrap_temp_from_page():
    from selenium import webdriver
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--window-size=1420,1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver', options=chrome_options)
    # used https://weerindelft.nl/WU/55ajax-dashboard-testpage.php
    # instead https://weerindelft.nl as more precise url allows me to download less data
    driver.get('https://weerindelft.nl/WU/55ajax-dashboard-testpage.php')
    p_element = driver.find_element_by_id(id_='ajaxtemp')
    return p_element.text

def format_temp(temp):
    return temp.strip().replace("°C", " degrees Celsius")


if __name__ == '__main__':
    unformated_temp = scrap_temp_from_page()
    print(format_temp(unformated_temp))