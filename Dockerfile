FROM python:3.6
WORKDIR /root/
COPY reqirements.txt /root/
RUN pip install -r reqirements.txt
COPY install_chrome_driver.sh /root/
RUN chmod +x install_chrome_driver.sh
RUN ./install_chrome_driver.sh
COPY HoeWarmIsHetInDelft.py /root/
CMD python HoeWarmIsHetInDelft.py